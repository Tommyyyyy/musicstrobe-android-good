package com.mediocrefireworks.musicstrobe;



import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;

/**
 * audioProcessor class: this handles the audio data and decides wether to flash
 * on each frequency
 * Created by thomaskiddle on 1/02/17.
 */

class AudioProcessor {

    static final int F = 8;
    private static final int[] logTable = new int[256];

    static {
        logTable[0] = logTable[1] = 0;
        for (int i = 2; i < 256; i++) logTable[i] = 1 + logTable[i / 2];
        logTable[0] = -1;
    }

    public boolean hasBeenCalibrated = false;
    public float inputFloat2 = 0.4f;
    //   float silenceOffset = 0.5f;
    float[] fCalibratedSilenceValues = DefaultValues.SILENCEVALUES;
    //  boolean siAutoOn = true;
    float[] fFreqs = new float[F];
    float[] fMeans = new float[F];
    float[] fSMeans = new float[F];
    float[] fSilenceMeans = new float[F];
    boolean[] fFlashed = new boolean[F];
    float[] fSliderVals = DefaultValues.FSLIDERTHRESHOLDS;
    boolean anyFlash = false;
    int bufferSize = 8;
    float allFTotalMean = 0;
    /**
     * silenceOffset is a slider in settings ranges from 0 - 1
     * fCalibratedSilenceValues typicals range from 0 - 1
     */
    boolean[] goingDown = {false, false, false, false, false, false, false, false};
    float[] temp = new float[F];
    boolean[] flashNow = new boolean[F];
    float[] veryMin = new float[F];
    float biggestDifference = 0;
    float volumeAdjust = 1;
    int count = 0;
    private float[] fTotals = new float[F];
    private float[] fSTotals = new float[F];
    private ArrayList<LinkedList<Float>> fBufferQueues = new ArrayList<LinkedList<Float>>();
    private ArrayList<LinkedList<Float>> fSBufferQueues = new ArrayList<LinkedList<Float>>();
    private int sBufferSize = 50;
    private MainActivity mainActivity;
    private int volumeAdjustCount = 0;


    AudioProcessor(MainActivity mainActivity) {

        this.mainActivity = mainActivity;

        for (int i = 0; i < F; ++i) {

            fTotals[i] = 0;
            fMeans[i] = 0;
            fFlashed[i] = false;
            //load array with a new queue
            fBufferQueues.add(new LinkedList<Float>());
            fSBufferQueues.add(new LinkedList<Float>());
            //load each f queue with 0's
            for (int x = 0; x < bufferSize - 1; ++x) {
                fBufferQueues.get(i).add(0f);
            }

            fSilenceMeans[i] = 0.2f;
            fSMeans[i] = 0.2f;
            fSTotals[i] = 0f;

            for (int x = 0; x < sBufferSize; x++) {
                fSBufferQueues.get(i).add(0.2f);
                fSTotals[i] += 0.2f;

            }
        }
//TODO changed those for loops to arrayfill
        Arrays.fill(temp, 0);
        Arrays.fill(flashNow, false);
        Arrays.fill(veryMin, 0.01f);
    }

    public static final int log2(float f) {
        int x = Float.floatToIntBits(f);
        int c = x >> 23;

        if (c != 0) return c - 127; //Compute directly from exponent.
        else //Subnormal, must compute from mantissa.
        {
            int t = x >> 16;
            if (t != 0) return logTable[t] - 133;
            else return (x >> 8 != 0) ? logTable[t] - 141 : logTable[x] - 149;
        }
    }

    void pullSoundData() {

        fFreqs = mainActivity.getVolumes();


        //reset anyFlash so not already set true
        anyFlash = false;
/*
        //get average volume of all volumes
        allFTotalMean = 0;
        for (int i = 0; i < F; i++) {

            if (fFreqs[i] > 0.0f) {
                allFTotalMean += fFreqs[i];
            }
        }
          allFTotalMean = allFTotalMean / F;

        //go through and find the biggest positive difference between the frequencies and the mean
        biggestDifference = 0;
        for(int i =0; i<F;i++){
            if(biggestDifference < fFreqs[i] - allFTotalMean){
                biggestDifference  = fFreqs[i] - allFTotalMean;
            }
        }
*/

        if (volumeAdjustCount <= 100 && fFreqs[4] > 0.0f) {
            volumeAdjust += 0.5f / fFreqs[4];


            if (volumeAdjustCount == 100) {
                volumeAdjust = volumeAdjust / 100;
            }

            //     Log.i("FUCK", "  volume Adjust" + volumeAdjust / volumeAdjustCount);
            volumeAdjustCount++;
                }

        for (int i = 0; i < F; ++i) {


            if (fFreqs[i] > 0.0f) {


                fFreqs[i] = fFreqs[i] * volumeAdjust;

                fBufferQueues.get(i).add(fFreqs[i]);
                fTotals[i] += fFreqs[i];
                fTotals[i] -= fBufferQueues.get(i).pop();
                fMeans[i] = (fTotals[i] / bufferSize);


                /**
                 * if current volume is quieter than the silence Mean * offset then we want
                 * to lower the silence mean. here we add the current frequency to the
                 * queue of silence mean values. if the current volume is louder then we
                 * raise the silence mean by a tiny amount.
                 */


                boolean testFlash = false;

                //siAutoOn
                if (true) {

                    if (fFreqs[i] < fSMeans[i]) {

                        fSBufferQueues.get(i).add(fFreqs[i]);
                        fSTotals[i] += fFreqs[i];
                        fSTotals[i] -= fSBufferQueues.get(i).pop();
                        fSMeans[i] = fSTotals[i] / sBufferSize;
                        //  if(i == 7){ Log.i("FUCK", "  going DOWN " + fSMeans[i]);}
                        goingDown[i] = true;


                    } else {

                        if (goingDown[i]) {
                            fSilenceMeans[i] = fSMeans[i];
                            // if(i == 7){ Log.i("FUCK", " Stopped doing down ---- " + fSMeans[i]);}

                            goingDown[i] = false;
                            testFlash = true;

                        } else {

                            fSBufferQueues.get(i).add(fSMeans[i] * (1.4f + 1));
                            fSTotals[i] += fSBufferQueues.get(i).getLast();
                            fSTotals[i] -= fSBufferQueues.get(i).pop();
                            fSMeans[i] = fSTotals[i] / sBufferSize;

                            // if(i == 7){ Log.i("FUCK", " going UP " + fSMeans[i]);}
                        }
                    }

                }

                fFreqs[i] = (fFreqs[i] * fSliderVals[i]);


                veryMin[i] = fCalibratedSilenceValues[i] * fSliderVals[i];
                temp[i] = ((fSilenceMeans[i] + fMeans[i]) / 2);

                /*

                    float relativeMeanAllFreq = allFTotalMean * (fCalibratedSilenceValues[i]);
                    temp = fFreqs[i] - relativeMeanAllFreq * 1.2f;

              */


                // decide if we should flash. makes sure current freq is higher than mean , min and the ignore level
                //todo can change this make it more effienct io freqs *silencemeans when we divide freqs by silence mean earlier

                flashNow[i] = false;

                //used to be this fFreqs[i] > fMeans[i] && fFreqs[i] > fSilenceMeans[i]

                if ((fFreqs[i] > temp[i] ||
                        (fFreqs[i] > fSilenceMeans[i] || fFreqs[i] > fMeans[i]))
                        && fFreqs[i] > veryMin[i]) {


                    if (!fFlashed[i]) {
                        // flashed makes sure we only flash 1 time when multiple frequencies want to
                        //flash on the same interation of all teh frequencies
                        //we want to
                        if (!anyFlash) {
                            //flash using a diferent dispatch queue as the sound listener !important
                            // self.flash()
                            anyFlash = true;
                        }

                        fFlashed[i] = true;
                        flashNow[i] = true;
                    }

                } else {
                    flashNow[i] = false;
                    fFlashed[i] = false;
                }
            }
        }

    }

    private float findMax(float... vals) {
        float max = 0;
        for (float f : vals) if (f > max) max = f;

        return max;
    }

}
