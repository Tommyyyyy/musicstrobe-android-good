package com.mediocrefireworks.musicstrobe;


import android.content.SharedPreferences;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;


public class CameraHelper {


    public Camera camera;
    public Parameters offParams;
    public Parameters onParams;
    public boolean isCameraOpen = false;
    private SharedPreferences settings;
    private SurfaceTexture st = new SurfaceTexture(1);

    public CameraHelper() {

    }

    public void openCamera() {

        if (!isCameraOpen) {
            if (camera == null) {
                try {
                    //Log.i("cam helper", "opening camera");
                    camera = Camera.open();

                    offParams = camera.getParameters();
                    onParams = camera.getParameters();
                    offParams.setFlashMode(Parameters.FLASH_MODE_OFF);
                    onParams.setFlashMode(Parameters.FLASH_MODE_TORCH);


                    camera.setPreviewTexture(st);
                    camera.startPreview();
                    isCameraOpen = true;

                    //Log.d("camera" , " created camera");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        //Log.i("cam helper", "opened camera finished camera is null :" + (camera == null));
    }


    public void releaseCamera() {

        try {
            if (camera != null) {
                camera.stopPreview();
                camera.release();
                camera = null;
                isCameraOpen = false;
                //Log.i("cam helper", "released camera which is null :" + (camera == null));
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }



    public void turnLEDOn(boolean turnOn) {

//
        if (camera != null && isCameraOpen) {
            if (turnOn) {
                camera.setParameters(onParams);
            } else {
                camera.setParameters(offParams);
            }
        }


    }


}
