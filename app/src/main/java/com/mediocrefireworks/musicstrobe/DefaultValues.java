package com.mediocrefireworks.musicstrobe;

/**
 * Created by thomaskiddle on 29/09/16.
 */

public class DefaultValues {

    public static final float SILENCEOFFSET = 0.5f;
    public static final float SILENCEVALUE = 0.1f;
    public static final float[] SILENCEVALUES = new float[]{SILENCEVALUE, SILENCEVALUE, SILENCEVALUE, SILENCEVALUE, SILENCEVALUE, SILENCEVALUE, SILENCEVALUE, SILENCEVALUE};
    public static final float[] FSLIDERTHRESHOLDS = new float[]{0.5f, 0.5f, 0.5f, 0.5f, 0.5f, 0.5f, 0.5f, 0.5f};
    public static final boolean AUTOSION = true;
    public static final int SILENCEOFFSET_MULTIPLIER = 1;


}
//