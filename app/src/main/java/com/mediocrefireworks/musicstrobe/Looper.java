package com.mediocrefireworks.musicstrobe;

import android.os.Handler;
import android.os.HandlerThread;
import android.widget.ImageView;

/**
 * Created by thomaskiddle on 25/09/16.
 */

public class Looper implements Runnable {


    public CameraHelper cameraHelper;
    boolean ledOn = false;
    private ImageView screenFlashOnIV;
    private int delay;
    private Runnable invalidateVMV;
    private boolean loopCanRun;
    private Handler h;
    private Handler uiHandler;
    private MainActivity mainActivity;
    private boolean screenOn = false;
    private boolean graphOn = false;
    private boolean loopHasStarted;
    private boolean flashNow = false;
    private VolumeMeterView vmv;
    private AudioProcessor ap;

    public Looper(Handler h, HandlerThread ht, Handler uiHandler, ImageView screenFlashOnIVi, MainActivity mainActivity) {

        cameraHelper = new CameraHelper();
        this.h = h;
        this.uiHandler = uiHandler;
        this.screenFlashOnIV = screenFlashOnIVi;
        this.mainActivity = mainActivity;
        this.delay = 20;
        loopCanRun = false;
        loopHasStarted = false;
        this.vmv = mainActivity.vmv;
        this.ap = mainActivity.ap;


        invalidateVMV = new Runnable() {
            @Override
            public void run() {
                vmv.invalidate();
            }
        };

    }

    @Override
    public void run() {
        if (loopCanRun) {
            h.postDelayed(this, delay);
            loopHasStarted = true;

            ap.pullSoundData();

            runOnUiThread(invalidateVMV);

            if (ap.anyFlash) {

                if (ledOn) {
                    cameraHelper.turnLEDOn(true);
                }

            } else {
                if (ledOn) {
                    cameraHelper.turnLEDOn(false);
                }
            }

        } else {
            //Log.d("looper" ,  " loop has stopped");
            //    Log.i("ASSHOLE", "xxxxxxxloop has STOPPED xxxxxxx");
            loopHasStarted = false;
            cameraHelper.turnLEDOn(false);
            //last run of looper down so can close camera
            cameraHelper.releaseCamera();
        }
    }


//


    public boolean isRunning() {
        if (loopCanRun) {
            return loopHasStarted;
        } else {
            return false;
        }
    }


    public void start() {
        if (!isRunning()) {
            loopCanRun = true;
            //Log.i("ASSHOLE", " --------loop has started ------");
            h.postDelayed(this, delay);
        }

    }

    public void stop() {
        loopCanRun = false;

        vmv.screenOn = false;
    }


    public void runOnUiThread(Runnable runnable) {

        uiHandler.post(runnable);
    }

}
