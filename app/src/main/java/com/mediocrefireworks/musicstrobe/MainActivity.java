package com.mediocrefireworks.musicstrobe;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;


public class MainActivity extends AppCompatActivity {


    private static final String TAG = "ASSHOLE";

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    private final int REQUEST_CAMERA = 1;
    private final int REQUEST_AUDIO = 2;

    private final int RESULT_SETTINGS = 12345;
    private final int RESULT_FIRSTCALIBRATE = 12346;
    // cant get toolbar to layout how I want so just creating manual methods for each toolbar button
    boolean ledBtnChecked = false;
    boolean screenBtnChecked = false;
    boolean graphBtnChecked = false;
    boolean silenceLevelActive = false;
    float[] x;
    SeekBar[] seekBars;
    VolumeMeterView vmv;
    AudioProcessor ap;
    //private CameraHelper cameraHelper;
    private Handler h;
    private Looper looper;
    private ImageView screenFlashOnIV;
    private SharedPreferences settings;
    private SharedPreferences.Editor editor;
    private AdView mAdView;
    private HandlerThread ht;
    private View mLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        System.loadLibrary("SuperpoweredSetup");

        //AD STUFF
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        //snackbar thing
        mLayout = findViewById(R.id.volumeMeterView);


        settings = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
        editor = settings.edit();
        	/*
             * wipe old settings when updating version
			 */

        int currentVersion = 0;
        try {
            currentVersion = this.getPackageManager()
                    .getPackageInfo(this.getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {

        }


        //last version is stored in settings from last upgrade
        final int lastVersion = settings.getInt("lastVersion", -1);
        if (currentVersion > lastVersion) {

            //Log.d(" fuck you " , " cleared cache");
            //cleared preferences
            editor.clear();
            editor.commit();
            //save the new version number
            editor.putInt("lastVersion", currentVersion);
            editor.commit();


        }
            /*
             * end of wipe settings
			 *
			 */


        ap = new AudioProcessor(this);
        loadSettings();

        //setup camera for flashing
        // cameraHelper = new CameraHelper();


        //screenFlashOffIV = (ImageView) findViewById(R.id.ledOffImageView);
        screenFlashOnIV = (ImageView) findViewById(R.id.ledOnImageView);
        screenFlashOnIV.setVisibility(View.INVISIBLE);

        vmv = (VolumeMeterView) findViewById(R.id.volumeMeterView);
        vmv.loadAudioProccesor(ap);

        //TODO:using handler thread does this make a diference?
        ht = new HandlerThread("Handler Thread for Sound Looper");
        ht.start();
        h = new Handler(ht.getLooper());

        looper = new Looper(h, ht, new Handler(), screenFlashOnIV, this);

        //hide LED flash button if phone has no flash
        //!note doesnt work properly on all phones, ie maybe return true when not true
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH)) {
            findViewById(R.id.ledBtn).setVisibility(View.GONE);
        }


        //DEBUG INPUTS  START

        SeekBar zoomMultiplierSeekBar = (SeekBar) findViewById(R.id.variableSeek2);
        zoomMultiplierSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                try {
                    ap.inputFloat2 = progress / 100f;
                    ap.inputFloat2 *= 2;
                    ap.inputFloat2 *= ap.inputFloat2;


                } catch (NumberFormatException e) {
                    //    Log.i("Exception", e.toString());

                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        //DEBUG INPUT END
        findViewById(R.id.graphView).setVisibility(View.GONE);


        RelativeLayout mainView = (RelativeLayout) findViewById(R.id.mainView);
        mainView.setOnTouchListener(new OnColorPickerListener(this, screenFlashOnIV) {
            @Override
            public boolean onTouch(View view, MotionEvent event) {

                if (!graphBtnChecked) {
                    if (event.getAction() == MotionEvent.ACTION_UP) {

                        vmv.changingFlashColor = false;
                    }
                    if (event.getAction() == MotionEvent.ACTION_MOVE) {

                        vmv.changingFlashColor = true;

                        vmv.changeFlashColor(this.colorAsInt);

                    }
                }

                return super.onTouch(view, event);
            }
        });


        seekBars = new SeekBar[8];

        seekBars[0] = (SeekBar) findViewById(R.id.mySeekBar0);
        seekBars[1] = (SeekBar) findViewById(R.id.mySeekBar1);
        seekBars[2] = (SeekBar) findViewById(R.id.mySeekBar2);
        seekBars[3] = (SeekBar) findViewById(R.id.mySeekBar3);
        seekBars[4] = (SeekBar) findViewById(R.id.mySeekBar4);
        seekBars[5] = (SeekBar) findViewById(R.id.mySeekBar5);
        seekBars[6] = (SeekBar) findViewById(R.id.mySeekBar6);
        seekBars[7] = (SeekBar) findViewById(R.id.mySeekBar7);


        int count = 0;
        for (final SeekBar seekBar : seekBars) {


            //load up seekbar progress if htey already there
            ap.fSliderVals[count] = settings.getFloat("sSlider" + count, DefaultValues.FSLIDERTHRESHOLDS[count]);


            seekBar.setTag(count);

            seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                public void onStopTrackingTouch(SeekBar bar) {
                    //save sliders
                    editor.commit();
                }

                public void onStartTrackingTouch(SeekBar bar) {

                }

                public void onProgressChanged(SeekBar bar, int paramInt, boolean paramBoolean) {

                    if (silenceLevelActive) {

                        int f = (int) seekBar.getTag();
                        //want to square the seek bar position
                        float temp = paramInt / 3f;
                        temp = temp * temp;

                        ap.fCalibratedSilenceValues[f] = (float) ((temp / 100f) + 0.01);
                        editor.putFloat("silence" + seekBar.getTag(), ap.fCalibratedSilenceValues[f]);
                        //   Log.i("FUCK", " slider " + f + "  :" + ((float) ((temp / 100f) + 0.01)));
                        //change color to show idea values
                        //reported class cast exception this isnt really neccesary so putting in try catch
                        try {
                            bar.getProgressDrawable().setColorFilter(sliderColor(paramInt), PorterDuff.Mode.SRC_IN);
                        } catch (Exception e) {

                        }

                    } else {

                        if (paramInt < 67 && paramInt > 63) {
                            bar.setProgress(65);
                        }

                        int f = (int) seekBar.getTag();
                        ap.fSliderVals[f] = (float) ((paramInt / 100f) + 0.01);
                        editor.putFloat("sSlider" + seekBar.getTag(), ap.fSliderVals[f]);

                        //change color to show idea values
                        //reported class cast exception this isnt really neccesary so putting in try catch
                        try {

                            bar.getProgressDrawable().setColorFilter(sliderColor(paramInt), PorterDuff.Mode.SRC_IN);
                        } catch (Exception e) {

                        }

                    }
                }
            });
            //loading values here to triger in progress changes methods to change the colour
            seekBar.setProgress((int) ((ap.fSliderVals[count] - 0.01) * 100));
            count++;
        }


        //STARTING LOOPER
        if (hasPermissionAudioRecord()) {
            loadSuperPowered();
        }

    }


    public void toggleSilenceVolumeLevels(View v) {
        ImageButton tb = (ImageButton) v;
        ImageButton rb = (ImageButton) findViewById(R.id.resetBtn);
        if (!silenceLevelActive) {
            silenceLevelActive = true;
            //load silence values to seek bars
            tb.setImageResource(R.drawable.ic_sliderspink);
            rb.setImageResource(R.drawable.ic_resetpink);


        } else {
            silenceLevelActive = false;
            tb.setImageResource(R.drawable.ic_slidersgreen);
            rb.setImageResource(R.drawable.ic_resetgreen);
        }


        for (final SeekBar seekBar : seekBars) {
            if (silenceLevelActive) {
                //need to reverse the squaring
                double temp = ((ap.fCalibratedSilenceValues[(int) seekBar.getTag()] - 0.01f) * 100);
                if (temp == 0) {
                    temp = 0;
                } else {
                    temp = Math.sqrt(temp);
                }
                temp = temp * 3;
                seekBar.setProgress((int) temp);
                LayerDrawable ld = (LayerDrawable) seekBar.getProgressDrawable();
                ld.setColorFilter(Color.MAGENTA, PorterDuff.Mode.SRC_IN);

            } else {
                seekBar.setProgress((int) ((ap.fSliderVals[(int) seekBar.getTag()] - 0.01) * 100));
                LayerDrawable ld = (LayerDrawable) seekBar.getProgressDrawable();
                ld.setColorFilter(sliderColor(seekBar.getProgress()), PorterDuff.Mode.SRC_IN);

            }
        }
    }


    public void resetSliders(View v) {


        for (final SeekBar seekBar : seekBars) {
            int f = (int) seekBar.getTag();
            if (silenceLevelActive) {
                //need to reverse the squaring
                //set the silence slider values to silence means as a reset
                ap.fCalibratedSilenceValues[f] = ap.fSilenceMeans[f];
                editor.putFloat("silence" + seekBar.getTag(), ap.fCalibratedSilenceValues[f]);
                double temp = ((ap.fCalibratedSilenceValues[f] - 0.01f) * 100);
                if (temp == 0) {
                    temp = 0;
                } else {
                    temp = Math.sqrt(temp);
                }
                temp = temp * 3;
                seekBar.setProgress((int) temp);
                LayerDrawable ld = (LayerDrawable) seekBar.getProgressDrawable();
                ld.setColorFilter(Color.MAGENTA, PorterDuff.Mode.SRC_IN);

            } else {

                ap.fSliderVals[f] = 0.5f;
                // Log.i("FAGGOT", " slider defaults " + DefaultValues.FSLIDERTHRESHOLDS[f]);
                //   System.out.println( " slider values "+ seekBar.getTag()+" " + ap.fSliderVals[f]);
                editor.putFloat("sSlider" + seekBar.getTag(), ap.fSliderVals[f]);
                seekBar.setProgress((int) ((ap.fSliderVals[f] - 0.01) * 100));
                LayerDrawable ld = (LayerDrawable) seekBar.getProgressDrawable();
                ld.setColorFilter(sliderColor(seekBar.getProgress()), PorterDuff.Mode.SRC_IN);

            }
        }

        //   System.out.println("commiting");
        editor.commit();


    }

    void loadSuperPowered() {
        // Get the device's sample rate and buffer size to enable low-latency Android audio output, if available.
        String samplerateString = null, buffersizeString = null;
        if (Build.VERSION.SDK_INT >= 17) {
            AudioManager audioManager = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
            samplerateString = audioManager.getProperty(AudioManager.PROPERTY_OUTPUT_SAMPLE_RATE);
            buffersizeString = audioManager.getProperty(AudioManager.PROPERTY_OUTPUT_FRAMES_PER_BUFFER);
        }
        if (samplerateString == null) samplerateString = "44100";
        if (buffersizeString == null) buffersizeString = "512";


        SuperpoweredSetup(Integer.parseInt(samplerateString), Integer.parseInt(buffersizeString));
    }


    public boolean hasPermissionAudioRecord() {
        return ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED;

    }


    public boolean hasPermissionCamera() {
        return ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;

    }

    /**
     * Requests the Camera permission.
     * If the permission has been denied previously, a SnackBar will prompt the user to grant the
     * permission, otherwise it is requested directly.
     */
    public void requestAudioPermission() {
        // Log.i(TAG, "AUDIO permission has NOT been granted. Requesting permission.");

        // BEGIN_INCLUDE(audio_permission_request)
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.RECORD_AUDIO)) {
            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example if the user has previously denied the permission.
            //    Log.i(TAG,
            //               "Displaying audio permission rationale to provide additional context.");
            Snackbar.make(mLayout, "With out Music Strobe being allowed to record audio, the app will not work.",
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction("OK", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ActivityCompat.requestPermissions(MainActivity.this,
                                    new String[]{Manifest.permission.RECORD_AUDIO},
                                    REQUEST_AUDIO);
                        }
                    })
                    .show();
        } else {

            // Camera permission has not been granted yet. Request it directly.
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO},
                    REQUEST_AUDIO);
        }
        // END_INCLUDE(audio_permission_request)
    }


    /**
     * Requests the Camera permission.
     * If the permission has been denied previously, a SnackBar will prompt the user to grant the
     * permission, otherwise it is requested directly.
     */
    public void requestCameraPermission() {
        // Log.i(TAG, "CAMERA permission has NOT been granted. Requesting permission.");

        // BEGIN_INCLUDE(camera_permission_request)
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.CAMERA)) {
            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example if the user has previously denied the permission.
            // Log.i(TAG,
            //          "Displaying camera permission rationale to provide additional context.");
            Snackbar.make(mLayout, "With out Music Strobe being allowed to use the camera, the flash will not work",
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction("OK", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ActivityCompat.requestPermissions(MainActivity.this,
                                    new String[]{Manifest.permission.CAMERA},
                                    REQUEST_CAMERA);
                        }
                    })
                    .show();
        } else {

            // Camera permission has not been granted yet. Request it directly.
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA},
                    REQUEST_CAMERA);
        }
        // END_INCLUDE(camera_permission_request)
    }


    public void screenBtnClick(View v) {

        //  Log.i(TAG, "screen checked" + screenBtnChecked);
        if (!screenBtnChecked) {

            //need to set this so can turn the button on, on callback
            screenBtnChecked = true;
            if (!hasPermissionAudioRecord()) {
                requestAudioPermission();
            } else {
                //wont start if already running
                looper.start();
                screenBtnOn();
            }

        } else {
            ((ImageView) v).setImageResource(R.drawable.ic_screenbtn);
            vmv.screenOn = false;
            screenBtnChecked = false;
            keepScreenOn(false);
            //    Log.i("TAG", "keep screen off ");
        }

    }

    public void screenBtnOn() {
        ImageView v = (ImageView) findViewById(R.id.screenBtn);
        v.setImageResource(R.drawable.ic_screenbtnon);
        vmv.screenOn = true;
        screenBtnChecked = true;
        keepScreenOn(true);
        //      Log.i("TAG", "keep screen on ");
    }

    public void ledBtnClick(View v) {

        if (!ledBtnChecked) {
            //need to set this here so can see if should be on when doing callback
            ledBtnChecked = true;
            if (!hasPermissionAudioRecord()) {
                requestAudioPermission();
            } else {

                if (hasPermissionCamera()) {
                    //         Log.i(TAG, "has camera permission");
                    //wont start if already running
                    looper.start();

                    ledBtnOn();
                } else {
                    requestCameraPermission();
                }
            }

        } else {
            //  Log.i(TAG, " led button off");
            looper.ledOn = false;
            //incase is stopped while flash is on
            looper.cameraHelper.turnLEDOn(false);
            ((ImageView) v).setImageResource(R.drawable.ic_flashbtn);
            ledBtnChecked = false;
            //   Log.i("TAG", "keep screen off ");
            keepScreenOn(false);
        }
    }

    void ledBtnOn() {
        ledBtnChecked = true;
        ImageView v = (ImageView) findViewById(R.id.ledBtn);
        looper.ledOn = true;
        v.setImageResource(R.drawable.ic_flashbtnon);
        keepScreenOn(true);
        // Log.i("TAG", "keep screen on ");
        try {
            looper.cameraHelper.openCamera();
        } catch (Exception e) {

        }
    }

    public void settingsBtnClick(View v) {

        Intent intent = new Intent(this, SettingsActivity.class);
        startActivityForResult(intent, RESULT_SETTINGS);
    }


    public void graphBtnClick(View v) {

        ImageButton b = (ImageButton) v;
        if (!graphBtnChecked) {
            graphBtnChecked = true;
            keepScreenOn(true);
            b.setImageResource(R.drawable.ic_graphbtnon);

            vmv.showGraph = true;
            findViewById(R.id.graphView).setVisibility(View.VISIBLE);
            if (!hasPermissionAudioRecord()) {
                requestAudioPermission();
            } else {
                //wont start if already running
                looper.start();

            }

        } else {
            graphBtnChecked = false;
            b.setImageResource(R.drawable.ic_graphbtn);
            //hide seekbars

            findViewById(R.id.graphView).setVisibility(View.GONE);
            vmv.showGraph = false;

            keepScreenOn(false);
        }

    }


    public void keepScreenOn(boolean on) {
        if (on) {
            //  System.out.println(" KEEP SCREEN ON");
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        } else {

            if (!graphBtnChecked && !screenBtnChecked && !ledBtnChecked) {
                //  System.out.println(" KEEPING SCREEN OFF");
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            }
        }
    }

    private void loadSettings() {
        //  ap.silenceOffset = settings.getFloat("silenceOffset", DefaultValues.SILENCEOFFSET) * DefaultValues.SILENCEOFFSET_MULTIPLIER;

        //   Log.i("Main Activity  ", " loaded silence offset*10 = " + ap.silenceOffset);
/*
        ap.fCalibratedSilenceValues = new float[8];
        for (int i = 0; i < 8; i++) {
            ap.fCalibratedSilenceValues[i] = settings.getFloat("silence" + i, DefaultValues.SILENCEVALUE);
            Log.i("Main Activity  ", " loaded settings:" + "  calibrated silence values " + i + ":" + ap.fCalibratedSilenceValues[i]);
        }
*/

        //  ap.siAutoOn = settings.getBoolean("autoSiOn", DefaultValues.AUTOSION);
        ap.hasBeenCalibrated = settings.getBoolean("silenceCalibrated", false);
        // Log.i("Main Activity  ", " loaded settings:" + " has been calibrated:" + ap.hasBeenCalibrated);
        //sliders get loaded inside the slider creating part
    }


    private int sliderColor(int param) {
        float val = param / 100f;
        float red = 0;
        float green = 0;
        float blue = 0.2f;

        if (val > 0.72) {
            red = (val - 0.72f) * 3.125f;
            green = (1 - val) * 3.125f;

        } else if (val <= 0.72 && val >= 0.58) {
            red = 0;
            green = 1;
        } else if (val < 0.58) {
            red = (0.58f - val) * 0.2f;
            green = (float) Math.pow((val * 1.7241), 2);
            blue = red;
        }


        int ired = (int) (red * 255);
        int igreen = (int) (green * 255);
        int iblue = (int) (blue * 255);

        return Color.argb(255, ired, igreen, iblue);

    }


    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        if (requestCode == REQUEST_CAMERA) {
            // BEGIN_INCLUDE(permission_result)
            // Received permission result for camera permission.
            //     Log.i(TAG, "Received response for Camera permission request.");

            // Check if the only required permission has been granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Camera permission has been granted, preview can be displayed
                //   Log.i(TAG, "CAMERA permission has now been granted.");
                Snackbar.make(mLayout, R.string.permision_available_camera,
                        Snackbar.LENGTH_SHORT).show();
                if (hasPermissionCamera()) {
                    ledBtnOn();
                }


            } else {
                //     Log.i(TAG, "CAMERA permission was NOT granted.");
                Snackbar.make(mLayout, R.string.permissions_not_granted,
                        Snackbar.LENGTH_SHORT).show();
                ledBtnChecked = false;

            }
            // END_INCLUDE(permission_result)

        } else if (requestCode == REQUEST_AUDIO) {
            //    Log.i(TAG, "Received response for contact permissions request.");

            // We have requested multiple permissions for contacts, so all of them need to be
            // checked.
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // All required permissions have been granted, display contacts fragment.
                /**  Snackbar.make(mLayout, "Audio permission has been granted",
                 Snackbar.LENGTH_SHORT)
                 .show();**/
                //        Log.i(TAG, " permission granted loading superpower and starting loop");
                loadSuperPowered();
                //always resumes when doing callback so starting in resume
                if (screenBtnChecked) {
                    //        Log.i(TAG, " screen on");
                    screenBtnOn();
                } else if (ledBtnChecked) {
                    //      Log.i(TAG, " check camrea permision ");
                    if (hasPermissionCamera()) {
                        //need to open caemra before using leds
                        try {
                            looper.cameraHelper.openCamera();
                        } catch (Exception e) {

                        }
                        ledBtnOn();
                    } else {
                        requestCameraPermission();
                    }
                }
            } else {
                //   Log.i(TAG, "audio permissions were NOT granted.");
                Snackbar.make(mLayout, R.string.permissions_not_granted,
                        Snackbar.LENGTH_SHORT)
                        .show();
                screenBtnChecked = false;
                ledBtnChecked = false;
            }

        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RESULT_SETTINGS) {

            loadSettings();

        }

    }


    @Override
    protected void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        onBackground();
        if (looper != null) {
            looper.stop();
            //      Log.i(TAG, "stopping looper");
        }

        //looper releases camera when stopping
        super.onPause();


    }

    @Override
    protected void onResume() {
        super.onResume();
        onForeground();
        //      Log.i(TAG, "resuming");
        if (looper != null && (screenBtnChecked || ledBtnChecked || graphBtnChecked)) {
            if (hasPermissionAudioRecord()) {
                //           Log.i(TAG, "starting looper");
                looper.start();

                //when resuming if a button is on then turn  back on that feature
                if (screenBtnChecked) {
                    screenBtnOn();
                }
                if (ledBtnChecked) {
                    if (hasPermissionCamera()) {
                        looper.cameraHelper.openCamera();
                        ledBtnOn();
                    }
                }

                if (graphBtnChecked) {
                    keepScreenOn(true);
                }
            }
        }

        if (mAdView != null) {
            mAdView.resume();
        }
    }

    @Override
    protected void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        looper.stop();
        stopSuperpowered();
        if (ht != null) {
            ht.quit();

        }
        super.onDestroy();
    }


    private native void SuperpoweredSetup(int samplerate, int buffersize);

    public native float[] getVolumes();

    //superpowered methods it says to call
    public native void onForeground();

    public native void onBackground();

    public native void stopSuperpowered();


    public SharedPreferences.Editor getEditor() {
        return editor;
    }


}
