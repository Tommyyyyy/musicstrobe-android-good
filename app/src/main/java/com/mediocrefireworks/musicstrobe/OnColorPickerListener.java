package com.mediocrefireworks.musicstrobe;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.view.Display;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;


/**
 * //
 * Created by thomaskiddle on 29/09/16.
 */
public abstract class OnColorPickerListener implements View.OnTouchListener {
    private final GestureDetector gestureDetector;

    public int colorAsInt = 0;
    public Paint fuckoff = new Paint();


    public OnColorPickerListener(Context context, ImageView iv) {


        gestureDetector = new GestureDetector(context, new OnColorPickerGestureListener(context, iv));
        gestureDetector.setIsLongpressEnabled(false);

        fuckoff.setColor(Color.WHITE);
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {

        return gestureDetector.onTouchEvent(event);

    }


    public class OnColorPickerGestureListener implements GestureDetector.OnGestureListener {

        int width;
        Context context;
        ImageView screenFlashOnIV;
        float currentColorPos = 0;
        boolean mainBarShown = true;

        public OnColorPickerGestureListener(Context context, ImageView screenFlashOnIV) {
            this.screenFlashOnIV = screenFlashOnIV;
            this.context = context;

            WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            width = size.x;
        }

        @Override
        public boolean onDown(MotionEvent e) {


            return true;
        }

        @Override
        public void onShowPress(MotionEvent e) {

        }

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            return false;
        }

        private int getColorFromPos(int i) {

            int m = 255;

            float itemp = ((float) i) / ((float) width);

            //	System.out.println(" itemp:" + itemp + " width:"+width+"  i:" + i);

            itemp = itemp * 5 * m;

            itemp = itemp * 1.5f - width * 0.5f;
            i = (int) itemp;

            int r = 255;
            int g = 255;
            int b = 255;


            if (i < 0) {

            } else if (i > 0 && i < m) {
                r = 255;
                g = 0;
                b = i;
            } else if (i > m && i < 2 * m) {
                r = 2 * m - i;
                g = 0;
                b = 255;
            } else if (i > 2 * m && i < 3 * m) {
                r = 0;
                g = i - 2 * m;
                b = 255;
            } else if (i > 3 * m && i < 4 * m) {
                r = 0;
                g = 255;
                b = 4 * m - i;
            } else if (i > 4 * m && i < 5 * m) {
                r = i - 4 * m;
                g = 255;
                b = 0;
            }

            //System.out.println(" r:" + r +" g:" + g +" b:" + b );
            return Color.rgb(r, g, b);
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float x, float y) {


            //make its horizontal movement
            if (Math.abs(x) > Math.abs(y)) {


                currentColorPos += x;
                if (currentColorPos > (width - 30)) {
                    currentColorPos = 0;
                }
                if (currentColorPos < 0) {
                    currentColorPos = (width - 30);

                }

                //screenFlashOnIV.getDrawable().setColorFilter(, PorterDuff.Mode.MULTIPLY);
                colorAsInt = getColorFromPos((int) (currentColorPos));
                //Log.d(" fuck you" ,"settings background color x:"+currentColorPos + " width " + width);
                return true;
            }
            return false;
        }

        @Override
        public void onLongPress(MotionEvent e) {

        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float vx, float vy) {

            boolean swipeY = Math.abs(vy) > Math.abs(vx);
            //System.out.println("  vy" + vy);
            if (swipeY && vy > 80 && !mainBarShown) {
                //show
                mainBarShown = true;
                //          mainActivity.findViewById(R.id.my_toolbar).setVisibility(View.VISIBLE);
//
            } else if (swipeY && vy < -80) {
                //hide
                mainBarShown = false;
                // mainActivity.findViewById(R.id.my_toolbar).setVisibility(View.GONE);

            }
            return true;
        }


    }
}
