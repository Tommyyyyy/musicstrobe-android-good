package com.mediocrefireworks.musicstrobe;

/**
 * Created by thomaskiddle on 29/09/16.
 */

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatDelegate;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.TreeSet;

//

/**
 * A {@link PreferenceActivity} that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 * <p>
 * See <a href="http://developer.android.com/design/patterns/settings.html">
 * Android Design: Settings</a> for design guidelines and the <a
 * href="http://developer.android.com/guide/topics/ui/settings.html">Settings
 * API Guide</a> for more information on developing a Settings UI.
 */
public class SettingsActivity extends Activity {

    private static final boolean ALWAYS_SIMPLE_PREFS = false;
    static int i = 0;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    boolean stopCalibrating = true;
    /**
     * Determines whether to always show the simplified settings UI, where
     * settings are presented in a single list. When false, settings are shown
     * as a master/detail two-pane view on tablets. When true, a single pane is
     * shown on tablets.
     */

    private boolean autoSiOn = true;
    private SharedPreferences settings;
    private SharedPreferences.Editor editor;
    private boolean silenceCalibratied = false;

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        setContentView(R.layout.activity_settings);


        settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        editor = settings.edit();


        System.loadLibrary("SuperpoweredSetup");
        //reset count

        //load check box with default or saved value
        autoSiOn = settings.getBoolean("autoSiOn", DefaultValues.AUTOSION);
        ((CheckBox) findViewById(R.id.silenceAutoCheckBox)).setChecked(autoSiOn);

            findViewById(R.id.calibrateProgressBar).setVisibility(View.VISIBLE);


        //    System.out.println(" GETTTINGGG autoSiOn " + autoSiOn);

        final SeekBar sensSB = (SeekBar) this.findViewById(R.id.senseLevelSB);
        int percentProgress = 0;

        percentProgress = (int) (settings.getFloat("silenceOffset", DefaultValues.SILENCEOFFSET) * 100);
        //Log.i("FUCK", " % progress" + percentProgress);
        sensSB.setProgress(percentProgress);
        TextView sensTV = (TextView) findViewById(R.id.senseLevelTV);
        sensTV.setText("BackGround Level Offset: " + percentProgress + "%");
        sensSB.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {


            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {

                float floatProgress = ((progress) / 100f);
                //Log.i("FUCK", " puting silenceOffset" + floatProgress);
                editor.putFloat("silenceOffset", floatProgress);

                TextView sensTV = (TextView) findViewById(R.id.senseLevelTV);
                sensTV.setText("Background Level Offset:" + (progress) + "%");

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });


    }



    public void calibrateSilenceBtn(View v) {

        final ProgressBar pg = ((ProgressBar) findViewById(R.id.calibrateProgressBar));

        final Button btn = (Button) v;


        //while calibrating
        //change text to silence image
        btn.setText("");


        final TreeSet<Float>[] silenceValBuffers = new TreeSet[8];
        final float[] fSilenceVals = new float[8];

        pg.getProgressDrawable().setColorFilter(Color.CYAN, PorterDuff.Mode.SRC_IN);
        pg.setProgress(0);
        pg.setMax(400);

        for (int i = 0; i < silenceValBuffers.length; i++) {
            silenceValBuffers[i] = new TreeSet<Float>();
        }

        i = 0;

        final Handler h = new Handler();
        final int delay = 2; //milliseconds
        final int totalRuns = 400;

        stopCalibrating = false;
        h.postDelayed(new Runnable() {
            public void run() {
                //if we want to interupt the calibrating loop
                if (!stopCalibrating) {
                    float[] silenceVols = getVolumes();
                    for (int x = 0; x < 8; x++) {
                        silenceValBuffers[x].add(silenceVols[x]);
                    }
                    pg.setProgress(i);
                    pg.getIndeterminateDrawable();
                    //finished
                    if (i == totalRuns) {
                        btn.setText("Calibrate");

                        pg.getProgressDrawable().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
                        int bufferFreqCount = 0;
                        // retrieve second to highest volume from each frequency
                        for (TreeSet<Float> silenceValBuf : silenceValBuffers) {
                            for (int x = 0; x < 2; x++) {
                                // need to poll off the last value before you can acces the next in line
                                fSilenceVals[bufferFreqCount] = (silenceValBuf.pollLast());
                            }
                            bufferFreqCount++;
                        }
                        saveSilenceValues(fSilenceVals);
                    } else {
                        h.postDelayed(this, delay);
                    }
                    i++;

                } else {
                    pg.setProgress(0);
                    btn.setText("Calibrate");
                }
            }
        }, delay);


    }


    public void saveSilenceValues(float[] fSilenceVals) {
        int f = 0;
        silenceCalibratied = true;
        editor.putBoolean("silenceCalibrated", true);
        for (float silenceVal : fSilenceVals) {

            //Log.d("settings " , "saving fSilence val: " + f +"  value:" + silenceVal );

            editor.putFloat("silence" + f, silenceVal);
            f++;

        }

    }


    public void autoSilenceBtn(View view) {


        stopCalibrating = true;
        //   System.out.println(" changeing btn " + autoSiOn);
        autoSiOn = !autoSiOn;

        //     System.out.println(" autoSiOn " + autoSiOn);
        editor.putBoolean("autoSiOn", autoSiOn);


    }


    @Override
    public void onBackPressed() {
        //  Log.i("FUCK", "comited settings");
        editor.commit();
        super.onBackPressed();

    }


    public void superpoweredImageClick(View view) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.superpowered.com"));
        startActivity(browserIntent);

    }

    public native float[] getVolumes();


}
