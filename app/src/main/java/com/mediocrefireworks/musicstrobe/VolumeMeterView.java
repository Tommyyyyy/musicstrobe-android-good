package com.mediocrefireworks.musicstrobe;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.ShapeDrawable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by thomaskiddle on 4/10/16.
 */

/**
 * must load audio processor before using
 */
public class VolumeMeterView extends View {


    public boolean showGraph = false;
    public boolean screenOn = false;
    public boolean changingFlashColor = false;
    Paint volPaint = new Paint();
    Paint meanPaint = new Paint();
    Paint silencePaint = new Paint();
    Paint whitePaint = new Paint();
    Paint flashPaint = new Paint();
    Paint yellowPaint = new Paint();
    Paint orangePaint = new Paint();
    Paint greenPaint = new Paint();
    AudioProcessor ap;
    float amplitutde = 0;
    Rect rect;
    float density;
    Bitmap canvasBitmap;
    Canvas canvas;
    ShapeDrawable shapeDrawable;
    //use ap instead of volumes
    int zoom = 64;

    public VolumeMeterView(Context context) {
        super(context);
        if (!isInEditMode()) {
            init(context);
        }
    }

    public VolumeMeterView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            init(context);
        }
    }

    public VolumeMeterView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        if (!isInEditMode()) {
            init(context);
        }
    }

    private void init(Context context) {
        rect = new Rect();
        density = getResources().getDisplayMetrics().density;
        shapeDrawable = new ShapeDrawable();
        volPaint.setColor(Color.DKGRAY);
        meanPaint.setColor(Color.CYAN);
        yellowPaint.setColor(Color.YELLOW);
        silencePaint.setColor(Color.MAGENTA);
        whitePaint.setColor(Color.WHITE);
        flashPaint.setColor(Color.WHITE);
        orangePaint.setColor(ContextCompat.getColor(context, R.color.Orange));
        greenPaint.setColor(ContextCompat.getColor(context, R.color.GreenYellow));
    }

    void loadAudioProccesor(AudioProcessor ap) {

        this.ap = ap;
        flashPaint.setColor(Color.WHITE);

    }

    //For drawing meter
    public void onDraw(Canvas c) {
        if (!isInEditMode()) {

            if (ap != null) {
                flashPaint.setColor(Color.WHITE);
                int barWidth = getWidth() / 8;

                if ((ap.anyFlash && screenOn) || changingFlashColor) {
                    c.drawRect(0, 0, getWidth(), getHeight(), flashPaint);
                }
                if (showGraph) {
                    int zoom = (int) (0.05f * getHeight() * ap.inputFloat2);

                    for (int i = 0; i < 8; i++) {
                        if (ap.flashNow[i]) {
                            c.drawRect((barWidth * i + 5), getHeight() - ap.fFreqs[i] * zoom, ((barWidth * (i + 1)) - 5), getHeight(), whitePaint);
                        } else {
                            c.drawRect((barWidth * i + 5), getHeight() - ap.fFreqs[i] * zoom, ((barWidth * (i + 1)) - 5), getHeight(), volPaint);
                        }


                        //  float mean = ap.fMeans[i];
                        //float yellow = ap.fSilenceMeans[i];
                        float silence = ap.veryMin[i];
                        float orange = ap.temp[i];
                        float green = ap.temp[i];

                        //   c.drawLine((barWidth * i + 5), getHeight() - green* zoom, ((barWidth * (i + 1)) - 5), getHeight() - green* zoom, greenPaint);

                        //  c.drawLine((barWidth * i + 5), getHeight() - mean * zoom, ((barWidth * (i + 1)) - 5), getHeight() - mean * zoom, meanPaint);

                        // c.drawLine((barWidth * i + 5), getHeight() - yellow * zoom, ((barWidth * (i + 1)) - 5), getHeight() - yellow * zoom, yellowPaint);
                        // c.drawLine((barWidth * i + 5), getHeight() - orange * zoom, ((barWidth * (i + 1)) - 5), getHeight() - orange * zoom, orangePaint);

                        c.drawLine((barWidth * i + 5), getHeight() - silence * zoom, ((barWidth * (i + 1)) - 5), getHeight() - silence * zoom, silencePaint);
                    }
                }
            }
        }

    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        //dont need this but will use it if we put in the oncolor picker listener here insteaed of insdie the main acitivity

        return super.onTouchEvent(event);
    }


    public void changeFlashColor(int color) {

        flashPaint.setColor(color);
    }


    public void invalidateVols() {
        invalidate();
    }


}
