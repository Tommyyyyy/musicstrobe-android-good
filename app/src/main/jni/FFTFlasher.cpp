//
// Created by Thomas Kiddle on 23/09/16.
//

#include "FFTFlasher.h"
#include <queue>
#include <string>

#define  LOG_TAG    "FFTFlasher"

#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)
#define  LOGW(...)  __android_log_print(ANDROID_LOG_WARN,LOG_TAG,__VA_ARGS__)
#define  LOGD(...)  __android_log_print(ANDROID_LOG_DEBUG,LOG_TAG,__VA_ARGS__)
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)

float total = 0;

// number of frequencies
const int f = 8;
int fBufferSize = 10;
// higher numbers create more flashing because all these numbers get divided into the mean making a lower mean
// and flashes more when freq is higher than mean

//ranges from 0.5 to 1.5

float fSliderVals[f] = {1.4, 1.1, 0.5, 0.5, 0.5, 1, 1, 1.2};

float fTotals[f];
float fMeans[f];
std::queue<float> fBufferQueues[f];
bool fFlashedOnce[f];
bool flashed = false;

//this is for sending means if flashed and volume
//float outputArrays[f*3+1];
std::vector<float> outputArrays(24);


FFTFlasher::FFTFlasher(void) {
    //load fbuffers with values


    for (int i = 0; i < f; ++i) {

        fTotals[i] = 0;
        fMeans[i] = 0;
        fFlashedOnce[i] = 0;
        //load each f queue with 0's
        for (int x = 0; x < fBufferSize - 1; ++x) {
            fBufferQueues[i].push(0);
        }


    }

}


std::vector<float> FFTFlasher::calculate(float cFreqs[]) {
    flashed = false;
    //for each buffer
    for (int i = 0; i < f; ++i) {

        fBufferQueues[i].push(cFreqs[i]);
        fTotals[i] += cFreqs[i];
        fTotals[i] -= fBufferQueues[i].front();
        fBufferQueues[i].pop();

        fMeans[i] = (fTotals[i] / fBufferSize) / sensitivityThreshold / fSliderVals[i];


        if (cFreqs[i] > fMeans[i] && cFreqs[i] > fSilenceVals[i] * 1.1) {
            /**we only want it to flash once when the vol is greater than the mean
            when vol goes back down lower than mean it can flash again
            this actually only saves 1 - 3 flashes but may enable the sense theshold
             to be set higher and not overload the thread with too many flashes
            **/
            if (!fFlashedOnce[i]) {
                // flashed makes sure we only flash 1 time when multiple frequencies want to
                //flash on the same interation of all teh frequencies

                if (!flashed) {
                    //flash using a diferent dispatch queue as the sound listener !important
                    flashed = true;
                }
                fFlashedOnce[i] = true;
            }
        }
        else {
            fFlashedOnce[i] = false;
        }


        //volumes
        outputArrays[0 + i] = cFreqs[i];
        //means
        outputArrays[8 + i] = fMeans[i];
        //flash
        outputArrays[16 + i] = fFlashedOnce[i];

        // outputArrays[24] = flashed;


    }


    //fake 2d array 0 * = volumes, 1 =  means, 2 = flash
    return outputArrays;


}






