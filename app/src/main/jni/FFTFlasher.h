//
// Created by Thomas Kiddle on 23/09/16.
//

#ifndef MUSICSTROBETRY2_FFTFLASHER_H
#define MUSICSTROBETRY2_FFTFLASHER_H


#include <vector>

class FFTFlasher {

public:
    FFTFlasher();

    std::vector<float> calculate(float cFreqs[]);

    float fSilenceVals[8];
    float sensitivityThreshold = 0.2;
    float fSliderVals[8];

};


#endif //MUSICSTROBETRY2_FFTFLASHER_H
