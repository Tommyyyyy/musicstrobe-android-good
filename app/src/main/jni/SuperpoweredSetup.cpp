#include <jni.h>
#include <stdlib.h>
#include <String>
#include <SLES/OpenSLES.h>
#include <SLES/OpenSLES_AndroidConfiguration.h>
#include <AndroidIO/SuperpoweredAndroidAudioIO.h>

#include "SuperpoweredBandpassFilterbank.h"
#include "SuperpoweredSimple.h"
#include "FFTFlasher.h"

#include "android/log.h"

#define  LOG_TAG    "SUPERPOWERED.CPP"

#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)
#define  LOGW(...)  __android_log_print(ANDROID_LOG_WARN,LOG_TAG,__VA_ARGS__)
#define  LOGD(...)  __android_log_print(ANDROID_LOG_DEBUG,LOG_TAG,__VA_ARGS__)
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)

static SuperpoweredAndroidAudioIO *audioIO;
static SuperpoweredBandpassFilterbank *filters;
static float bands[8];
static pthread_mutex_t mutex;
static unsigned int samplerate, samplesProcessedForOneDisplayFrame;
static float *inputBufferFloat;


FFTFlasher fftFlasher;


#define FFT_LOG_SIZE 11 // 2^11 = 2048

// This is called periodically by the media server.
static bool audioProcessing(void *__unused clientdata, short int *audioInputOutput,
                            int numberOfSamples, int __unused samplerate) {

    SuperpoweredShortIntToFloat(audioInputOutput, inputBufferFloat,
                                (unsigned int) numberOfSamples); // Converting the 16-bit integer samples to 32-bit floating point.
    //float interleaved[numberOfSamples * 2 + 16];

    //TODO: dont know what to put for first 2 params
    // SuperpoweredInterleave(inputBufferFloat, inputBufferFloat, interleaved, numberOfSamples);

    float peak, sum;
    pthread_mutex_lock(&mutex);
    samplesProcessedForOneDisplayFrame += numberOfSamples;
    filters->process(inputBufferFloat, bands, &peak, &sum, numberOfSamples);
    pthread_mutex_unlock(&mutex);

    return false;

}


// this is were I'm initialising everything
extern "C" JNIEXPORT void Java_com_mediocrefireworks_musicstrobe_MainActivity_SuperpoweredSetup(
        JNIEnv *env, jobject __unused obj, jint samplerate, jint buffersize) {


    float frequencies[8] = {55, 110, 220, 440, 880, 1760, 3520, 7040};
    float widths[8] = {1, 1, 1, 1, 1, 1, 1, 1};
    filters = new SuperpoweredBandpassFilterbank(8, frequencies, widths, samplerate);
    inputBufferFloat = (float *) malloc(buffersize * sizeof(float) * 2 + 128);
    audioIO = new SuperpoweredAndroidAudioIO(samplerate, buffersize, true, false, audioProcessing,
                                             NULL, -1, SL_ANDROID_STREAM_MEDIA,
                                             buffersize * 2); // Start audio input/output.




}


static float cFreqs[8];


extern "C" JNIEXPORT jfloatArray Java_com_mediocrefireworks_musicstrobe_MainActivity_getVolumes(
        JNIEnv *env, jobject __unused obj) {

    // this is run at screen refresh rate of about 60fps or 20mhz
    //wether screen should flash should be decided in this method



    pthread_mutex_lock(&mutex);
    if (samplesProcessedForOneDisplayFrame > 0) {
        for (int n = 0; n < 8; ++n) {
            cFreqs[n] = (((bands[n] / samplesProcessedForOneDisplayFrame)) * 1024);
        }

        memset(bands, 0, 8 * sizeof(float));
        samplesProcessedForOneDisplayFrame = 0;


    } else memset(cFreqs, 0, 8 * sizeof(float));
    pthread_mutex_unlock(&mutex);


    jfloatArray vols = (*env).NewFloatArray(8);
    (*env).SetFloatArrayRegion(vols, 0, 8, cFreqs);


    return vols;

}


extern "C" JNIEXPORT jfloatArray Java_com_mediocrefireworks_musicstrobe_SettingsActivity_getVolumes(
        JNIEnv *env, jobject __unused obj) {


    pthread_mutex_lock(&mutex);
    if (samplesProcessedForOneDisplayFrame > 0) {
        for (int n = 0; n < 8; ++n) {
            cFreqs[n] = (((bands[n] / samplesProcessedForOneDisplayFrame)) * 1024);
        }

        memset(bands, 0, 8 * sizeof(float));
        samplesProcessedForOneDisplayFrame = 0;


    } else memset(cFreqs, 0, 8 * sizeof(float));
    pthread_mutex_unlock(&mutex);


    jfloatArray vols = (*env).NewFloatArray(8);
    (*env).SetFloatArrayRegion(vols, 0, 8, cFreqs);


    return vols;
}


extern "C" JNIEXPORT jfloatArray Java_com_mediocrefireworks_musicstrobe_CalibrateActivity_getVolumes(
        JNIEnv *env, jobject __unused obj) {


    pthread_mutex_lock(&mutex);
    if (samplesProcessedForOneDisplayFrame > 0) {
        for (int n = 0; n < 8; ++n) {
            cFreqs[n] = (((bands[n] / samplesProcessedForOneDisplayFrame)) * 1024);
        }

        memset(bands, 0, 8 * sizeof(float));
        samplesProcessedForOneDisplayFrame = 0;


    } else memset(cFreqs, 0, 8 * sizeof(float));
    pthread_mutex_unlock(&mutex);


    jfloatArray vols = (*env).NewFloatArray(8);
    (*env).SetFloatArrayRegion(vols, 0, 8, cFreqs);


    return vols;
}



extern "C" JNIEXPORT void Java_com_mediocrefireworks_musicstrobe_MainActivity_updateSettings(
        JNIEnv *env, jobject __unused obj, jfloat senseThreshold, jfloatArray silenceVals) {
    fftFlasher.sensitivityThreshold = senseThreshold;

    for (int i = 0; i < 8; i++) {
        float *elements = env->GetFloatArrayElements(silenceVals, 0);
        fftFlasher.fSilenceVals[i] = elements[i];
        //  LOGD(" superpower setup loading silence values: %F", fftFlasher.fSilenceVals[i]);
        env->ReleaseFloatArrayElements(silenceVals, elements, 0);
    }
}


extern "C" JNIEXPORT void Java_com_mediocrefireworks_musicstrobe_MainActivity_updatefSliders(
        JNIEnv *env, jobject __unused obj, jfloatArray fSliderValss) {


    for (int i = 0; i < 8; i++) {
        float *elements = env->GetFloatArrayElements(fSliderValss, 0);
        fftFlasher.fSliderVals[i] = elements[i];

        //   LOGD(" f: %d", i);
        LOGD(" superpower setup loading slider values: %F", fftFlasher.fSliderVals[i]);

        env->ReleaseFloatArrayElements(fSliderValss, elements, 0);
    }


}



extern "C" JNIEXPORT void Java_com_mediocrefireworks_musicstrobe_MainActivity_onForeground(
        JNIEnv *env, jobject __unused obj) {

    if (audioIO) {
        audioIO->onForeground();
    }

}
extern "C" JNIEXPORT void Java_com_mediocrefireworks_musicstrobe_MainActivity_onBackground(
        JNIEnv *env, jobject __unused obj) {
    if (audioIO) {
        audioIO->onBackground();
    }
}
extern "C" JNIEXPORT void Java_com_mediocrefireworks_musicstrobe_MainActivity_stopSuperpowered(
        JNIEnv *env, jobject __unused obj) {
    if (audioIO) {
        audioIO->stop();
    }
}